//
//  ViewController.swift
//  Assignment2
//
//  Created by MacStudent on 2019-06-26.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController,WCSessionDelegate {
    
   
    @IBOutlet weak var SenderLabel: UILabel!
    @IBOutlet weak var senderText: UITextField!
    
    @IBAction func buttonClicked(_ sender: UITextField) {
        
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let message = ["Phone":senderText.text]
            // send the message to the watch
            WCSession.default.sendMessage(message,replyHandler: nil)
        }

    }
    
  
  
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
       
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("checking to see if wc session is supported")
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()

        
    }


}

}
