//
//  InterfaceController.swift
//  Assignment2 WatchKit Extension
//
//  Created by MacStudent on 2019-06-26.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController,WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
     //   print("Got a message!")
        
        // update the message with a label
        myLabel.setText("\(message)")
    }

    
   

    @IBOutlet weak var myLabel: WKInterfaceLabel!
    @IBAction func buttonPressed() {
        
      //  print("i pushed the button")
        
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let message1 = ["Message": "Message set from Phone is received by Watch"]

            // send the message to the watch
            WCSession.default.sendMessage(message1,replyHandler: nil)
        }
        
    
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print("Hello")
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }

        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
